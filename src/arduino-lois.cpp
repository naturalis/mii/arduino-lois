// Created for: Naturalis Biodiversity Center
// Created by: Frank Vermeij & Titus Kretzschmar
// License: CC BY-SA 4.0

#include <Keyboard.h>

#define Inp_0 A0
#define Inp_1 A1
#define Inp_2 A2
#define Inp_3 A3
#define Inp_4 A4
#define Inp_5 A5
#define Inp_6 1
#define Inp_7 0
#define KEY0 'z'
#define KEY1 'x'
#define KEY2 'c'
#define KEY3 'v'
#define KEY4 'b'
#define KEY5 'n'
#define KEY6 'm'
#define KEY7 ','
#define PRESS_DELAY 50

#define Led 13
#define DEBUG 0


void setup() {
  if (DEBUG > 0) {
    Serial.begin(115200);
  }

  pinMode(Inp_0,INPUT_PULLUP);
  pinMode(Inp_1,INPUT_PULLUP);
  pinMode(Inp_2,INPUT_PULLUP);
  pinMode(Inp_3,INPUT_PULLUP);
  pinMode(Inp_4,INPUT_PULLUP);
  pinMode(Inp_5,INPUT_PULLUP);
  pinMode(Inp_6,INPUT_PULLUP);
  pinMode(Inp_7,INPUT_PULLUP);

  pinMode(Led,OUTPUT);
  digitalWrite(Led,LOW); // Led uit
}

void CheckInputs(void)
{
  digitalWrite(Led,LOW); // Led uit

  if (digitalRead(Inp_0) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY0);  // send a 'z' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 1");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY0);
  }

  if (digitalRead(Inp_1) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY1);  // send a 'x' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 2");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY1);
  }
  if (digitalRead(Inp_2) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY2);  // send a 'c' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 3");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY2);
  }
  if (digitalRead(Inp_3) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY3);  // send a 'v' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 4");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY3);
  }
  if (digitalRead(Inp_4) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY4);  // send a 'b' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 5");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY4);
  }
  if (digitalRead(Inp_5) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY5);  // send a 'n' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 6");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY5);
  }
  if (digitalRead(Inp_6) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY6);  // send a 'm' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 7");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY6);
  }
  if (digitalRead(Inp_7) == LOW) {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY7);  // send a 'm' to the computer via Keyboard HID
    if (DEBUG > 0) {
      Serial.println("Input 8");  // Debug hulp
    }
  } else {
    Keyboard.release(KEY7);
  }
}


void loop() {
  CheckInputs();
  delay(PRESS_DELAY);
}
